package app

import (
	"fmt"
	"reflect"
	"testing"
)

func TestAppInstantiation(t *testing.T) {
	// instantiate a new App object
	a := NewApp()
	// ensure we have non-nil object
	if a == nil {
		t.Fail()
	}
	// ensure we have an object of the correct type
	if fmt.Sprintf("%s", reflect.TypeOf(a)) != "*app.App" {
		t.Fail()
	}
}
