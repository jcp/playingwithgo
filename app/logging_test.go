package app

import "testing"

func TestConfigurationLogging(t *testing.T) {
	c := &LoggingConfig{}
	c.LogFile = "a file name"
	if c.LogFile != "a file name" {
		t.Fail()
	}
}
