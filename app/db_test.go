package app

import "testing"

func TestConfigurationMySQL(t *testing.T) {
	c := &MySQLConfig{}
	c.Username = "username"
	c.Password = "password"
	c.Host = "host"
	c.Port = 3306
	c.Database = "database"
	if c.Username != "username" ||
		c.Password != "password" ||
		c.Host != "host" ||
		c.Port != 3306 ||
		c.Database != "database" {
		t.Fail()
	}
}
