package app

import (
	// mysql driver loaded as blank import
	"database/sql"
	"fmt"
	"log"
	"os"

	// load blank mysql driver
	_ "github.com/go-sql-driver/mysql"
)

// MySQLConfig bits for mysql setup
type MySQLConfig struct {
	Username string
	Password string
	Host     string
	Port     int
	Database string
}

func (a *App) initDB() {
	cfg := a.config.MySQL
	dsn := fmt.Sprintf("%s:%s@(%s:%d)/%s",
		cfg.Username,
		cfg.Password,
		cfg.Host,
		cfg.Port,
		cfg.Database)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	row := db.QueryRow("select VERSION() version")
	var sqlversion string
	err = row.Scan(&sqlversion)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	log.Println("Connected to MySQL version", sqlversion)

	a.db = db
}
