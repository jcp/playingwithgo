package app

import (
	"log"
	"os"
)

// LoggingConfig struct to store logging config info
type LoggingConfig struct {
	LogFile string
}

// InitLogging sets up the logger
func (a *App) initLogging() {
	cfg := a.config.Logging
	f, err := os.OpenFile(cfg.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("error opening file %s: %v", cfg.LogFile, err)
	}
	log.SetOutput(f)
	log.Println("initialized logging")
}
