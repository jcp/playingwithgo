package app

import (
	"encoding/json"
	"errors"
	"log"
	"os"
)

// Configuration is a struct containing config items
type Configuration struct {
	Logging    *LoggingConfig
	MySQL      *MySQLConfig
	WebService *WebServiceConfig
}

func (a *App) initConfig() {
	filename := os.Getenv("GO_APP_CONFIG")
	if filename == "" {
		err := errors.New("The GO_APP_CONFIG environment variable is not set")
		configurationError(err)
	}
	file, err := os.Open(filename)
	if err != nil {
		configurationError(err)
	}
	decoder := json.NewDecoder(file)

	configuration := Configuration{}
	err = decoder.Decode(&configuration)
	if err != nil {
		configurationError(err)
	}
	a.config = &configuration
}

func configurationError(err error) {
	log.Println("Failed to load configuration")
	log.Fatal(err)
}
