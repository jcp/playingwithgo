package app

import "testing"

func TestConfigurationWebService(t *testing.T) {
	c := &WebServiceConfig{}
	c.Address = "address"
	c.Port = 80
	c.WebRoot = "some path"
	if c.Address != "address" ||
		c.Port != 80 ||
		c.WebRoot != "some path" {
		t.Fail()
	}
}
