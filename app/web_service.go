package app

import (
	"fmt"
	"log"
	"net"
	"net/http"
)

// WebService - object for handlign thw web service stuff
type WebService struct {
	app      *App
	config   *WebServiceConfig
	listener *net.Listener
}

// WebServiceConfig - config struct for web service things
type WebServiceConfig struct {
	Address string
	Port    int
	WebRoot string
}

func (a *App) initWebService() {
	http.Handle("/", http.FileServer(http.Dir(a.config.WebService.WebRoot)))
	webservice := &WebService{
		a,
		a.config.WebService,
		nil,
	}
	a.webservice = webservice
}

func (w *WebService) listenAndServe() {
	cfg := w.config
	hostport := fmt.Sprintf("%s:%d", cfg.Address, cfg.Port)
	listener, err := net.Listen("tcp", hostport)
	if err != nil {
		log.Fatal("Error opening port for listening:", err.Error())
	}
	w.listener = &listener
	log.Printf("Listening for HTTP on %s", hostport)
	log.Fatal(http.Serve(*w.listener, nil))
}
