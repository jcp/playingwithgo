package app

import (
	"database/sql"
)

// App - the primary application object
type App struct {
	config     *Configuration
	db         *sql.DB
	webservice *WebService
}

// NewApp function returns a new app object pointer
// exported for use by external main() func
func NewApp() *App {
	return &App{}
}

// Launch function actually starts application
// exported for use by external main() func
func (a *App) Launch() {
	a.initConfig()
	a.initLogging()
	a.initDB()
	a.initWebService()

	// when web serves, it sits in a loop waiting for connections.
	// this should either be the last statement in this routine, or
	// it should be launched in its own go routine
	a.webservice.listenAndServe()
}
