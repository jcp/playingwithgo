package main

import (
	"./app"
)

func main() {
	myapp := app.NewApp()
	myapp.Launch()
}
