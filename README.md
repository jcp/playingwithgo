# Playing With Go
Because golang is kind of fun, and maybe we can make something fun with it.
If not, maybe we learn something at least.

## Work in Progress
This should be considered, at almost all times, an incomplete work in progress,
which may have functional pieces but will likely never be considered complete.

## Boredom Warning
This is likely to be pretty dull for anybody other than myself until such a
time that this project has morphed into something more fun than the nothing
it currently is.

## Diary
I'm attempting to keep a diary of the various challenges and thoughts during
this work.

* [Diary Entry 1 - A New Project (2016-09-16)](diary/entry1.md)
* [Diary Entry 2 - Application Configuration (2016-09-16)](diary/entry2.md)
* [Diary Entry 3 - Logging (2016-09-20)](diary/entry3.md)
* [Diary Entry 4 - MySQL (2016-09-25)](diary/entry4.md)
* [Diary Entry 5 - Testing (2016-09-25)](diary/entry5.md)
* [Diary Entry 6 - Cleanup (2016-09-25)](diary/entry6.md)
* [Diary Entry 7 - Web Service (2016-09-25)](diary/entry7.md)
