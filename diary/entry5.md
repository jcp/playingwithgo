# Playing with Go - Testing
**Entry 5 - September 25, 2016**

## Testing

Our application doesn't have a whole lot yet, but before it gets too big
we should consider including test cases for go's "go test" functionality.

To create an initial test for our application, create a new file
app/app_test.go:

```
package app

import (
	"fmt"
	"reflect"
	"testing"
)

func TestAppInstantiation(t *testing.T) {
	// instantiate a new App object
	a := NewApp()
	// ensure we have non-nil object
	if a == nil {
		t.Fail()
	}
	// ensure we have an object of the correct type
	if fmt.Sprintf("%s", reflect.TypeOf(a)) != "*app.App" {
		t.Fail()
	}
}

func TestConfigurationLogging(t *testing.T) {
	c := &Configuration{}
	c.LogFile = "test a"
	if c.LogFile != "test a" {
		t.Fail()
	}
}

func TestConfigurationMySQL(t *testing.T) {
	c := &MySQLConfig{}
	c.Username = "username"
	c.Password = "password"
	c.Host = "host"
	c.Port = 3306
	c.Database = "database"
	if c.Username != "username" ||
		c.Password != "password" ||
		c.Host != "host" ||
		c.Port != 3306 ||
		c.Database != "database" {
		t.Fail()
	}
}
```

This creates a few tests.
1. instantiate a new App object using NewApp function, test fails if result is
nil or wrong type.
2. create a new Configuration struct and ensure it will hold our necessary
logfile value
3. create new MySQLConfig struct and ensure it will hold values for the
necessary mysql configuration elements

At this point, we can't test Launch, initConfig, initDB, or initLogging
easily, because those depend on configured file paths, network ports, etc.. We
should be able to write more complex tests which actually generated a test
config file with a suitable test logfile and test mysql information.  But that
is more than I care to dive into at the moment.

As we create new functionality, we should try to remember to add new tests
when appropriate.

After saving the above file, I can run tests like so:

```
$ go test -v ./app
=== RUN   TestAppInstantiation
--- PASS: TestAppInstantiation (0.00s)
=== RUN   TestConfigurationLogging
--- PASS: TestConfigurationLogging (0.00s)
=== RUN   TestConfigurationMySQL
--- PASS: TestConfigurationMySQL (0.00s)
PASS
ok      gitlab.com/jcp/playingwithgo/app        0.055s
```

That's the extreme basics of how to add testing to a go package!
