# Playing with Go - Cleanup
**Entry 6 - September 25, 2016**

## Cleanup

At this point, there are some things we can do to tidy up before adding further
features.

### Parallelize Logging Config

First, let's give Logging its own configuration struct to keep things parallel.
Set it up in app/config.go like so:

```
// Configuration is a struct containing config items
type Configuration struct {
	Logging *LoggingConfig
	MySQL   *MySQLConfig
}
```

Then edit app/logging.go and create the struct:

```
// LoggingConfig struct to store logging config info
type LoggingConfig struct {
	LogFile string
}
```

This then requires that we adjust the initLogging() function, also in
app/logging.go.  All we need to do is account for the fact that what was
a.config.LogFile is now a.config.Logging.LogFile.  In this case I created a
pointer cfg to a.config.Logging to make for less typing later in the function.

```
// InitLogging sets up the logger
func (a *App) initLogging() {
	cfg := a.config.Logging
	f, err := os.OpenFile(cfg.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("error opening file %s: %v", cfg.LogFile, err)
	}
	log.SetOutput(f)
	log.Println("initialized logging")
}
```

We then need to edit our $GO_APP_CONFIG to adhere to the new config structure:

```
{
        "Logging": {
                "LogFile": "C:\\Users\\john\\Documents\\app.log"
        },
        "MySQL": {
                "Username": "playingwithgo",
                "Password": "yourdbpassword",
                "Host": "localhost",
                "Port": 3306,
                "Database": "playingwithgo"
        }
}
```

Finally, edit app/app_test.go and remove the TestConfigurationLogging function,
which will be replaced in the next section below.

### Spread out Testing

Also in app/app_test.go, remove TestConfigurationMySQL.  This leaves just the
App instantiation test in app_test.go.

Now create app/db_test.go:

```
package app

import "testing"

func TestConfigurationMySQL(t *testing.T) {
	c := &MySQLConfig{}
	c.Username = "username"
	c.Password = "password"
	c.Host = "host"
	c.Port = 3306
	c.Database = "database"
	if c.Username != "username" ||
		c.Password != "password" ||
		c.Host != "host" ||
		c.Port != 3306 ||
		c.Database != "database" {
		t.Fail()
	}
}
```

Then, create app/logging_test.go:

```
package app

import "testing"

func TestConfigurationLogging(t *testing.T) {
	c := &LoggingConfig{}
	c.LogFile = "a file name"
	if c.LogFile != "a file name" {
		t.Fail()
	}
}
```

Now go go test produces the same output as previously:

```
$ go test -v ./app
=== RUN   TestAppInstantiation
--- PASS: TestAppInstantiation (0.00s)
=== RUN   TestConfigurationMySQL
--- PASS: TestConfigurationMySQL (0.00s)
=== RUN   TestConfigurationLogging
--- PASS: TestConfigurationLogging (0.00s)
PASS
ok      gitlab.com/jcp/playingwithgo/app        0.040s
```

That's it for the housekeeping!
