# Playing with Go - MySQL
**Entry 4 - September 25, 2016**

## MySQL

We will need some sort of database to store data.  In this case, we will setup
a mysql connection.

First, create yourself a mysql database with a username and password for use
with this app.

Then we setup configuration bits our app will need to connect.  We edit our
$GO_APP_CONFIG file:

```
{
        "LogFile": "C:\\Users\\john\\Documents\\app.log",
        "MySQL": {
          "Username": "playingwithgo",
          "Password": "yourdbpassword",
          "Host": "localhost",
          "Port": 3306,
          "Database": "playingwithgo"
        }
}
```

First, we need to edit app/config.go and teach it that there is a
MySQL config structure.

```
// Configuration is a struct containing config items
type Configuration struct {
	LogFile string
	MySQL   *MySQLConfig
}
```

Now we can edit app/app.go to make a few changes:

First, we have to include sql.  Note log is being removed at the same time
because the line that uses it in Launch function is being removed below.

```
import (
	"database/sql"
)
```

Then add a sql element to the App struct.

```
// App - the primary application object
type App struct {
	config *Configuration
	db     *sql.DB
}
```

Then, also in app/app.go, we add a call to initDB() in the launch function.

```
// Launch function actually starts application
// exported for use by external main() func
func (a *App) Launch() {
	a.initConfig()
	a.initLogging()
	a.initDB()
}
```

Then we create this new app/db.go file:

```
package app

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	// mysql driver loaded as blank import
	_ "github.com/go-sql-driver/mysql"
)

// MySQLConfig bits for mysql setup
type MySQLConfig struct {
	Username string
	Password string
	Host     string
	Port     int
	Database string
}

func (a *App) initDB() {
	cfg := a.config.MySQL
	dsn := fmt.Sprintf("%s:%s@(%s:%d)/%s",
		cfg.Username,
		cfg.Password,
		cfg.Host,
		cfg.Port,
		cfg.Database)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	row := db.QueryRow("select VERSION() version")
	var sqlversion string
	err = row.Scan(&sqlversion)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	log.Println("Connected to MySQL version", sqlversion)

	a.db = db
}
```

Note that this file defines MySQLConfig which is now needed by config.go,
and initDB() function for App objects needed by app.go.

initDB generates a DataSource Name (user:pass@(host:port)/database) and
attempts to open a database connection.  If successful, it will indicate
as much in the log.  Otherwise, the application will error out.

Remember how I removed the "initialized config and logging" message from the
Launch function?  I didn't want that function to get overwhelmed logging info
for every init.  We can restore the functionality by tweaking the
app/logging.go file to indicate logging has been initialized:

```
// InitLogging sets up the logger
func (a *App) initLogging() {
	f, err := os.OpenFile(a.config.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("error opening file %s: %v", a.config.LogFile, err)
	}
	log.SetOutput(f)
	log.Println("initialized logging")
}
```

Now logging and db init functions announce their success.  There is no point
in doing so for config because configuration has to be loaded before logging
can be initialized, so there is no configured logger yet to use when config
init completes.

At this point, running the application generates the following in the log file:

```
2016/09/25 13:10:16 initialized logging
2016/09/25 13:10:16 Connected to MySQL version 5.7.10-log
```

The application now has a db object it can use to access the mysql database.
