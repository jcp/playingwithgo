# Playing with Go - Logging
**Entry 3 - September 20, 2016**

## Logging

We can't just have our app throwing everything at stderr and stdout.

For starters, let's ditch useless title from config and replace it with
something more useful.

In app/config.go now:

```
type Configuration struct {
	LogFile string
}
```

Now to edit our configuration file to match:

```
{
        "LogFile": "C:\\Users\\john\\Documents\\app.log"
}
```

While we're growing out the app package a bit, let's reduce the effort needed
in main.go and shift more of the tedious app-specific work into app itself.

I say this because we face this choice:

1. Have main() continue to micro-manage components within app, adding calls to
logging setup right after after config initialization.

2. Have a primary app function for main() to call, which, itself, handles the
various app components like configuration and logging.

We're going with #2.

First, create app/app.go:

```
package app

// App - the primary application object
type App struct {
	config *Configuration
}

// NewApp function returns a new app object pointer
// exported for use by external main() func
func NewApp() *App {
	return &App{}
}

// Launch function actually starts application
// exported for use by external main() func
func (a *App) Launch() {
	a.initConfig()
	a.initLogging()
}
```

This basically creates the App object class for us, which main() can instantiate
and launch.  From within this part of the app class, we have access to all
objects/functions within the class.  Outside of app package, such as in main()
function, only the app package objects/functions starting with capital letters
are available.  This is why NewApp and Launch must be capitalized, for main() to
have access to them.  For the msot part, everything else which is only accessed
by other parts of the app package, can be given names starting with lowercase
letters, such as initConfig, and initLogging.

Two big changes of note in the above app.go file:

1. The configuration object now has a place to be stored, as an element within
the App class object itself.  This will make it easy to access from just about
anywhere else in the app package.

2. initConfig is now lowercase, and treated as functions of
App type.  This is to make accessing and storing data easier with less parameter
passing and other effort.  We will have to make modifications to both the config
and the logging script to accomodate this usage.

In app/config.go, the LoadConfig function needs adjustment:

```
func (a *App)initConfig() {
	filename := os.Getenv("GO_APP_CONFIG")
	if filename == "" {
		err := errors.New("The GO_APP_CONFIG environment variable is not set")
		configurationError(err)
	}
	file, err := os.Open(filename)
	if err != nil {
		configurationError(err)
	}
	decoder := json.NewDecoder(file)

	configuration := Configuration{}
	err = decoder.Decode(&configuration)
	if err != nil {
		configurationError(err)
	}
	a.config = &configuration
}
```

In the first line, the function is:

1. a part of the App type/class now
2. now starts with a lowercase letter
3. no longer returns any value

And then at the bottom, where the return line used to be, is now assignment of
app.config to the new config object that has been generated.

And we should create our app/logging.go file with the initLogging function:

```
package app

import (
	"log"
	"os"
)

// InitLogging sets up the logger
func (a *App) initLogging() {
	f, err := os.OpenFile(a.config.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("error opening file %s: %v", a.config.LogFile, err)
	}
	log.SetOutput(f)
}
```

You can now clean up main() in main.go:

```
package main

import (
	"./app"
)

func main() {
	myapp := app.NewApp()
	myapp.Launch()
}
```

May as well test this with a tiny addition to app/app.go:

```
func (a *App) Launch() {
  a.initConfig()
  a.initLogging()
  log.Println("initialized config and logging")
}
```

Run the code and take comfort in seeing the message appear in the logfile
instead of the terminal.
