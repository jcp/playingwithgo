# Playing with Go - Application Configuration
**Entry 2 - September 16, 2016**

## Configuration

We need a method of configuring our application. Something to tell it what
addresses/ports to bind to, what endpoints to use, etc..

Let's use an environment variable to point the application at its configuration
file.  Something like GO_APP_CONFIG...

Let us create a config file:

```
NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$ cat > ~/Documents/go_app_config.json
{
        "title": "Playing With Go"
}

NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$
```

Now let us set an environment variable appropriately.

```
NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$ export GO_APP_CONFIG=~/Documents/go_app_config.json

NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$
```

Okay, now we just need to train our application to use this information.

We'll create an app package for our application use use, and create the
configuration code as part of the app package.

Let's create the app/ directory, and, within it, a file with our code
for configuration management.

So app/config.go gets created as:

```
package app

import (
	"encoding/json"
	"errors"
	"log"
	"os"
)

// Configuration is a struct containing config items
type Configuration struct {
	Title string
}

/* Read configuration file into config object */
func LoadConfig() *Configuration {
	filename := os.Getenv("GO_APP_CONFIG")
	if filename == "" {
		err := errors.New("The GO_APP_CONFIG environment variable is not set")
		configurationError(err)
	}
	file, err := os.Open(filename)
	if err != nil {
		configurationError(err)
	}
	decoder := json.NewDecoder(file)

	configuration := Configuration{}
	err = decoder.Decode(&configuration)
	if err != nil {
		configurationError(err)
	}
	return &configuration
}

func configurationError(err error) {
	log.Println("Failed to load configuration")
	log.Fatal(err)
}
```

A few things to note about the above:

1. This file contains all our configuration reading logic and allows an object
populated with configuration data to be instantiated
2. Near the top, is the Configuration type definition, essentially the object
the configuration info will be stored in.  Since we only put a "title" field into
the configuration file, that's all we have for now in our Configuration type.
3. LoadConfig is the function which instantiates/populates a Configuration type
object.
3. Most of the code in the LoadConfig function addresses the many possible
causes for failure to read the configuration, in which cases the
configurationError function is called and the application will exit.
4. errors.New() is used to generate our own custom error that we can throw
around and handle like any other error.

The main.go file now needs to be edited to look like this:

```
package main

import (
	"fmt"

	"./app"
)

func main() {

	/* our initial test */
	fmt.Println("Hello, world")

	// out config test
	config := app.LoadConfig()
	fmt.Println(config.Title)
}
```

Note here what we've done to main.go:
1. we now import ./app package
2. main function now generates a configuration object
3. main function then displays the configured value for "title"

Output from running the app now indicates that the configuration is being read:

```
NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$ go run ./main.go
Hello, world
Playing With Go

NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$
```

Configuration exists, let's call this one a victory and commit/push the changes!
