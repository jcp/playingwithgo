# Playing with Go - A New Project
**Entry 1 - September 16, 2016**

## A GitLab Account

I started this off by creating a gitlab account to use for hosting this
project's source code.  It seemed a suitable place to host this diary as
well.  I could have gone with github, but I'm comfortable with gitlab.  And
so here I am with my free gitlab account.

## Create a project

I had to create this project as a public project in gitlab.

Then, I cloned it into a suitable location in my go path.

This means, on my windows machine:
```
~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo
```

I take advantage of the git bash shell and its ssh tools to handle git on
Windows 10 (ssh-agent and ssh-add setup in bash profile script), same as I
would use it on a UNIX-like system.

## Create Something That Compiles

It's always good to start with something very simple and work your way up.
When it comes to Go, this should be as simple as you ever need to get.

```
package main

import (
        "fmt"
)

func main() {
        fmt.Println("Hello, world")
}
```

The above is certainly simple, and fortunately, therefore, likely to compile.
If you have go properly installed already then you shouldn't have any trouble.

```
NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$ go run ./main.go
Hello, world

NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$
```

## Success!

Well that's probably the hardest part of the whole project.  At this point it
is all about adding features until we're happy. =)

Better save our work so far!

```
NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$ git add main.go

NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$ git commit -m "initial commit of bare main.go"
[master (root-commit) ea65533] initial commit of bare main.go
 1 file changed, 9 insertions(+)
 create mode 100644 main.go

NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$ git push
Counting objects: 3, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 296 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To git@gitlab.com:jcp/playingwithgo.git
 * [new branch]      master -> master

NITBOX MINGW64 ~/Documents/code/gocode/src/gitlab.com/jcp/playingwithgo (master)
$
```

Our current revision is now saved to the origin repo on gitlab for viewing on
the web, pulling into other repos, etc.. Hurrah!
