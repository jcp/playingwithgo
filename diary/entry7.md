# Playing with Go - Web Service
**Entry 7 - September 25, 2016**

## Web Service

It's about time we started working on a way for users to interact with our
application.  In this case, we're  going with a web browser interface.

We'll need some new configuration items, our config file has grown to something
like this:

```
{
        "Logging": {

                "LogFile": "C:\\Users\\john\\Documents\\app.log"
        },
        "MySQL": {
                "Username": "playingwithgo",
                "Password": "yourdbpassword",
                "Host": "localhost",
                "Port": 3306,
                "Database": "playingwithgo"
        },
        "WebService" {
                "Address": "localhost",
                "Port": 8088,
                "WebRoot": "C:\\Users\\john\\Documents\\code\\gocode\\src\\gitlab.com\\jcp\\playingwithgo\\webroot"
        }
}
```

Now we have to tell config to use it, so edit app/config.go:

```
// Configuration is a struct containing config items
type Configuration struct {
	Logging    *LoggingConfig
	MySQL      *MySQLConfig
	WebService *WebServiceConfig
}
```

We should now make ourselves a new app/web_service.go file:

```
package app

import (
	"fmt"
	"log"
	"net"
	"net/http"
)

// WebService - object for handlign thw web service stuff
type WebService struct {
	app      *App
	config   *WebServiceConfig
	listener *net.Listener
}

// WebServiceConfig - config struct for web service things
type WebServiceConfig struct {
	Address string
	Port    int
	WebRoot string
}

func (a *App) initWebService() {
	http.Handle("/", http.FileServer(http.Dir(a.config.WebService.WebRoot)))
	webservice := &WebService{
		a,
		a.config.WebService,
		nil,
	}
	a.webservice = webservice
}

func (w *WebService) listenAndServe() {
	cfg := w.config
	hostport := fmt.Sprintf("%s:%d", cfg.Address, cfg.Port)
	listener, err := net.Listen("tcp", hostport)
	if err != nil {
		log.Fatal("Error opening port for listening:", err.Error())
	}
	w.listener = &listener
	log.Printf("Listening for HTTP on %s", hostport)
	log.Fatal(http.Serve(*w.listener, nil))
}
```

This file provides the WebService struct/type itself, as well as the config
struct for web service things.  Additionally, an initWebService function
which does a minimal amount of setup, and a listenAndServe function which
actually fires up the web service are provided.

We initWebService as an App type function, and it expects the webservice element
to exist in the App struct.

So let's edit that over in app/app.go:

```
// App - the primary application object
type App struct {
	config     *Configuration
	db         *sql.DB
	webservice *WebService
}
```

Further down in app.go, let's have our web service actually get utilized.

```
// Launch function actually starts application
// exported for use by external main() func
func (a *App) Launch() {
	a.initConfig()
	a.initLogging()
	a.initDB()
	a.initWebService()

	// when web serves, it sits in a loop waiting for connections.
	// this should either be the last statement in this routine, or
	// it should be launched in its own go routine
	a.webservice.listenAndServe()
}
```

And, finally, create a test file app/web_service_test.go:

```
package app

import "testing"

func TestConfigurationWebService(t *testing.T) {
	c := &WebServiceConfig{}
	c.Address = "address"
	c.Port = 80
	c.WebRoot = "some path"
	if c.Address != "address" ||
		c.Port != 80 ||
		c.WebRoot != "some path" {
		t.Fail()
	}
}
```

Testing with the verbose flag shows output for our new test:

```
$  go test -v ./app
=== RUN   TestAppInstantiation
--- PASS: TestAppInstantiation (0.00s)
=== RUN   TestConfigurationMySQL
--- PASS: TestConfigurationMySQL (0.00s)
=== RUN   TestConfigurationLogging
--- PASS: TestConfigurationLogging (0.00s)
=== RUN   TestConfigurationWebService
--- PASS: TestConfigurationWebService (0.00s)
PASS
ok      gitlab.com/jcp/playingwithgo/app        0.046s
```

Now create we create a new webroot/ directory, then a webroot/js/ directory.

Grab a recent version of jquery and put it into webroot/js, I used curl in
the Git bash shell like so:

```
$ curl -o jquery-3.1.1.min.js https://code.jquery.com/jquery-3.1.1.min.js
```

Now create a webroot/js/app.js file:

```
// a global var for our app
var app;

var App = function() {
  console.log("app instantiating");  
}
```

And let's also create webroot/js/main.js:

```
// runs when page is loaded
jQuery(function() {
  app = new App();
  alert('Hello, world');
});
```

And we now need a webroot/index.html file:

```
<html>
  <head>
    <title>Playing With Go</title>

    <script src="js/jquery-3.1.1.min.js"></script>

    <script src="js/app.js"></script>
    <script src="js/main.js"></script>
  </head>
  <body>
  </body>
</html>
```

Upon running the app (go run ./main.go), the log now indicates that the app
is listening on a port.

As indicated in comments, serving http is a process which involves an infinite
loop, so the app no longer exits, instead running indefinitely serving http
requests.

I am able to browse to http://localhost:8088/ and see the popup alert message
which is coded into webroot/js/main.js.  A look at Chrome's dev tools shows
the javascript console with the message indicate App object was instantiated.

We now have a web service, but all the content is static.  Next time, we'll
need to get dynamic client/server communications established.
